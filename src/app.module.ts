import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppController } from "./app.controller";

import { FeatureModule, TranslationModule } from "./modules";
import { SocketService } from "./socket.service";
import { AuthModule } from "./modules/auth/auth.module";
import { UserModule } from "./modules/user/user.module";

const MONGODB_HOST = process.env.MONGODB_HOST || "localhost";
const MONGODB_PORT = process.env.MONGODB_PORT || 27017;

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${MONGODB_HOST}:${MONGODB_PORT}/Blog`),
    FeatureModule,
    TranslationModule,
    AuthModule,
    UserModule
  ],
  controllers: [AppController],
  components: [SocketService]
})
export class ApplicationModule {}
