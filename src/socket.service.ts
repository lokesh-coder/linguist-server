import {
  WebSocketGateway,
  SubscribeMessage,
  WsResponse,
  WebSocketServer,
  WsException
} from "@nestjs/websockets";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/from";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";

import { FeatureService } from "./modules/feature/feature.service";

export interface WebsocketData {
  key: "FEATURE_FEEDS" | "TRANSLATION_FEEDS" | "default";
  data: {
    type: string;
    value: any;
  };
}

@WebSocketGateway()
export class SocketService {
  @WebSocketServer() server;

  emit(data: WebsocketData) {
    this.server.emit("SERVER_TO_CLIENT", data);
  }

  @SubscribeMessage("connection")
  onConnect(client, data): void {
    console.log("CONNECTED!");
  }

  @SubscribeMessage("CLIENT_TO_SERVER")
  onEvent(client, data): any {
    console.log("CLIENT_TO_SERVER!", data);

    // return Observable.fromPromise(this.featureService.getAll()).map(res => ({
    //   event: "events",
    //   data: res
    // }));
  }
}
