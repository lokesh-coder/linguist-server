import { readFileSync } from "fs";
import { createServer } from "https";
import { NestFactory } from "@nestjs/core";
import { ValidationPipe } from "@nestjs/common";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import * as cors from "cors";
import * as express from "express";
import { ApplicationModule } from "./app.module";

if ((process.env["NODE_ENV"] = "production")) {
  process.env["CLIENT_URL"] = "http://35.200.158.118";
} else {
  process.env["CLIENT_URL"] = "http://localhost:8765";
}

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule, {
    httpsOptions: {
      key: readFileSync("./cert/localhost.key"),
      cert: readFileSync("./cert/localhost.cert"),
      requestCert: false,
      rejectUnauthorized: false
    }
  });
  app.useGlobalPipes(new ValidationPipe());
  app.use(
    cors({
      origin: process.env["CLIENT_URL"],
      credentials: true
    })
  );
  app.use("/public", express.static("public"));
  const options = new DocumentBuilder()
    .setTitle("Linguist")
    .setDescription("Translations API")
    .setVersion("1.0")
    .addTag("feature")
    .addBearerAuth("Authorization", "header")
    .setSchemes("https")
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("/api", app, document);
  await app.listen(3000);
}
bootstrap();
