import * as passport from "passport";
import {
  Module,
  NestModule,
  MiddlewaresConsumer,
  RequestMethod
} from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "./passport/jwt.strategy";
import { AuthController } from "./auth.controller";

@Module({
  imports: [],
  components: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService]
})
export class AuthModule implements NestModule {
  public configure(consumer: MiddlewaresConsumer) {
    consumer
      .apply(passport.authenticate("jwt", { session: false }))
      .forRoutes(
        { path: "/feature/*", method: RequestMethod.ALL },
        { path: "/translation/*", method: RequestMethod.ALL }
      );
  }
}
