import * as jwt from "jsonwebtoken";
import { Component, Inject } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

@Component()
export class AuthService {
  private static readonly SECRET = "FOOBARBAZ";
  constructor() {}

  async createToken(userData) {
    const expiresIn = 60 * 60;
    const secretOrKey = AuthService.SECRET;
    const token = jwt.sign({}, secretOrKey, { expiresIn });
    return {
      expires_in: expiresIn,
      access_token: token
    };
  }

  async validateUser(signedUser): Promise<boolean> {
    // put some validation logic here
    // for example query user by id / email / username
    console.log("user data", signedUser);
    return true;
  }
}
