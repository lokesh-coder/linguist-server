import {
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Get,
  Res,
  Body
} from "@nestjs/common";
import { AuthService } from "./auth.service";

@Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("token")
  @HttpCode(HttpStatus.OK)
  async getToken() {
    return await this.authService.createToken({});
  }

  @Get("authorized")
  async authorized() {
    console.log("Authorized route...");
  }
}
