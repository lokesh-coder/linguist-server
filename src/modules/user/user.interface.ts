import { Document } from "mongoose";

export interface User extends Document {
  username: string;
  email: string;
  readonly type: string;
  readonly salt: string;
  readonly hash: string;
}
