import * as jwt from "jsonwebtoken";
import { Component, Inject, HttpException, HttpStatus } from "@nestjs/common";
import { UserSchema } from "./user.schema";
import { InjectModel } from "@nestjs/mongoose";
import { User } from "./user.interface";
import { Model } from "mongoose";

@Component()
export class UserService {
  constructor(
    @InjectModel(UserSchema) private readonly userModel: Model<User>
  ) {}

  async register(data) {
    let user = new this.userModel();
    user.username = data.username;
    user.email = data.email || "";
    (user as any).setPassword(data.password);
    return await user.save();
  }

  async login(data) {
    return await this.userModel
      .findOne({
        username: data.username
      })
      .exec()
      .then((user: any) => {
        if (!user)
          throw new HttpException(
            "Invalid credentials",
            HttpStatus.BAD_REQUEST
          );
        return user.validPassword(data.password) ? user : false;
      });
  }
}
