import {
  IsString,
  IsInt,
  IsNumber,
  IsArray,
  IsDate,
  IsOptional
} from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class UserDto {
  @IsString()
  @ApiModelProperty()
  readonly username: string;

  @IsOptional()
  @IsString()
  @ApiModelPropertyOptional()
  readonly email: string;

  @IsString()
  @ApiModelProperty()
  readonly hash: string;

  @IsString()
  @ApiModelProperty()
  readonly salt: string;

  @IsString()
  @ApiModelProperty()
  readonly type: string;
}

export class UserLoginDto {
  @IsString()
  @ApiModelProperty()
  readonly username: string;

  @IsOptional()
  @IsString()
  @ApiModelPropertyOptional()
  readonly email: string;

  @IsString()
  @ApiModelProperty()
  readonly password: string;
}

export class UserRegisterDto {
  @IsString()
  @ApiModelProperty()
  readonly username: string;

  @IsOptional()
  @IsString()
  @ApiModelPropertyOptional()
  readonly email: string;

  @IsString()
  @ApiModelProperty()
  readonly password: string;
}
