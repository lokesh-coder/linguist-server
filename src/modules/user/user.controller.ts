import {
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Get,
  Res,
  Body,
  Response,
  HttpException
} from "@nestjs/common";
import { UserService } from "./user.service";
import { UserDto, UserLoginDto, UserRegisterDto } from "./user.dto";
import { AuthService } from "../auth/auth.service";
import { ApiUseTags } from "@nestjs/swagger";

@ApiUseTags("user")
@Controller("user")
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService
  ) {}

  @Post("register")
  @HttpCode(HttpStatus.OK)
  async register(@Body() body: UserRegisterDto, @Res() res) {
    await this.userService
      .register(body)
      .then(x => this.authService.createToken(x))
      .then(ctx => res.json(ctx));
  }

  @Post("login")
  async login(@Body() body: UserLoginDto, @Res() res) {
    this.userService
      .login(body)
      .then(ctx => {
        if (!ctx)
          throw new HttpException(
            "Invalid credentials",
            HttpStatus.BAD_REQUEST
          );
        return this.authService.createToken(ctx);
      })
      .then(token => res.json(token))
      .catch(err => {
        console.log("err.status", err);
        res.status(err.status).json(err);
      });
  }
}
