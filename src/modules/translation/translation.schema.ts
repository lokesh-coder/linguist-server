import * as mongoose from "mongoose";
import { FeatureSchema } from "../feature/feature.schema";

const TranslationSchema = new mongoose.Schema(
  {
    featureId: { type: mongoose.Schema.Types.ObjectId, ref: "Feature" },
    text: { type: String, index: true },
    key: { type: String, index: true },
    en: { type: String, index: true },
    nl: { type: String, index: true },
    draft: Boolean
  },
  { timestamps: true }
);

TranslationSchema.index({ "$**": "text" });

TranslationSchema.post("save", (data, next: any) => {
  console.log("Saved", data);
  next();
});
TranslationSchema.post("findOneAndUpdate", (data, next: any) => {
  console.log("Updated", data);
  next();
});
TranslationSchema.post("findOneAndRemove", (data, next: any) => {
  console.log("Removed", data);
  next();
});

export { TranslationSchema };
