import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Translation } from "./translation.interface";
import { TranslationDto } from "./translation.dto";
import { TranslationSchema } from "./translation.schema";
import { InjectModel } from "@nestjs/mongoose";
import { FeatureSchema } from "../feature/feature.schema";
import { Feature } from "../feature/feature.interface";
import { TranslationExport } from "./translation.export";

@Component()
export class TranslationService {
  constructor(
    @InjectModel(TranslationSchema)
    private readonly translationModel: Model<Translation>,
    @InjectModel(FeatureSchema) private readonly featureModel: Model<Feature>,
    private translationExport: TranslationExport
  ) {}
  async getOne(id) {
    return await this.translationModel.findById(id).exec();
  }
  async getAll() {
    return await this.translationModel.find().exec();
  }
  async getAllWithFeature() {
    return await this.translationModel
      .find({})
      .populate("featureId")
      .exec();
  }
  async search(keyword) {
    return await this.translationModel
      .find(
        {
          $or: [
            { $text: { $search: keyword, $caseSensitive: false } },
            { nl: { $regex: keyword, $options: "i" } },
            { en: { $regex: keyword, $options: "i" } },
            { text: { $regex: keyword, $options: "i" } }
          ]
        },
        { score: { $meta: "textScore" } }
      )
      .sort({ score: { $meta: "textScore" } })
      .populate("featureId")
      .limit(5)
      .exec();
  }
  async export(lang) {
    return await this.translationModel
      .find({})
      .populate("featureId", "key")
      .exec()
      .then(data => this.translationExport.export(data, lang));
  }
  async update(id, data: Translation) {
    data.draft = !data.en || !data.nl;
    return await this.translationModel
      .findByIdAndUpdate(id, data, { new: true })
      .exec()
      .then(async data => await this.updateDraft(data));
  }
  async deleteOne(id) {
    return await this.translationModel
      .findByIdAndRemove(id)
      .exec()
      .then(async data => await this.decrementCount(data));
  }
  async deleteAll() {
    return await this.translationModel
      .find()
      .remove()
      .exec();
  }
  async save(data: TranslationDto): Promise<Translation> {
    let translationData = new this.translationModel(data);
    return await translationData
      .save()
      .then(async data => await this.incrementCount(data));
  }
  async getAllUnderFeature(fid) {
    return await this.translationModel
      .find()
      .where("featureId")
      .equals(fid)
      .exec();
  }
  private incrementCount(data) {
    return this.featureModel
      .findById(data.featureId)
      .exec()
      .then(feature => {
        feature.translations.push(data._id);
        if (!data.en || !data.nl) {
          feature.draftTranslations.push(data._id);
        }
        console.log("updating count...");
        feature.save();
        return data;
      });
  }
  private decrementCount(data) {
    return this.featureModel
      .findById(data.featureId)
      .exec()
      .then(feature => {
        feature.translations = feature.translations.filter(x => x != data._id);
        feature.draftTranslations = feature.draftTranslations.filter(
          x => x != data._id
        );
        console.log("decrementing count...");
        feature.save();
        return data;
      });
  }
  private updateDraft(data) {
    return this.featureModel
      .findById(data.featureId)
      .exec()
      .then(feature => {
        if (!data.en || !data.nl) {
          feature.draftTranslations.push(data._id);
        } else {
          feature.draftTranslations = feature.draftTranslations.filter(
            x => x != data._id
          );
        }
        console.log("updating drafts...");
        feature.save();
        return data;
      });
  }
}
