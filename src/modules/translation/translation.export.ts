import { Component } from "@nestjs/common";

@Component()
export class TranslationExport {
  export(data, lang: "en" | "nl" = "en") {
    let enTranslations = {};
    let nlTranslations = {};

    data.forEach(t => {
      let k = [t.featureId.key, t.key].join("_");
      enTranslations[k] = t.en;
      nlTranslations[k] = t.nl;
    });
    if (lang == "en") return enTranslations;
    if (lang == "nl") return nlTranslations;
  }
}
