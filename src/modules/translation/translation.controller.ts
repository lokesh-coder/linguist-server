import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Query,
  Res,
  Body,
  NotFoundException,
  HttpStatus
} from "@nestjs/common";
import { TranslationService } from "./translation.service";
import { TranslationDto } from "./translation.dto";
import {
  ApiUseTags,
  ApiOperation,
  ApiProduces,
  ApiImplicitParam,
  ApiImplicitHeader
} from "@nestjs/swagger";
import { Types, Schema } from "mongoose";
import { SocketService } from "../../socket.service";
import { TranslationSocket } from "./translation.socket";

@ApiUseTags("translation")
@Controller("translation")
export class TranslationController {
  constructor(
    private readonly translationService: TranslationService,
    private socket: TranslationSocket
  ) {}
  @Get()
  root(): string {
    return "Hello =>translation controller!";
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/one/:id")
  async getOne(@Param("id") id: string, @Res() res) {
    this.translationService
      .getOne(id)
      .then(ctx => res.json(ctx))
      .catch(err => {
        res.status(HttpStatus.NOT_FOUND).send();
      });
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/all")
  async getAll(@Res() res) {
    this.translationService.getAll().then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/feature/all")
  async getAllWithFeature(@Res() res) {
    this.translationService.getAllWithFeature().then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/feature/:fid")
  async getAllUnderFeature(@Param("fid") fid: string, @Res() res) {
    this.translationService.getAllUnderFeature(fid).then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Post("/save")
  async save(@Body() body: TranslationDto, @Res() res) {
    this.translationService
      .save(body)
      .then(x => this.socket.added(x))
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Put("/one/:id")
  async update(
    @Param("id") id: string,
    @Body() body: TranslationDto,
    @Res() res
  ) {
    this.translationService
      .update(id, body)
      .then(x => this.socket.updated(x))
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Delete("/one/:id")
  async delete(@Param("id") id: string, @Res() res) {
    this.translationService
      .deleteOne(id)
      .then(x => this.socket.deleted(x))
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Delete("/all")
  async deleteAll(@Res() res) {
    this.translationService.deleteAll().then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/export/:lang")
  async export(@Param("lang") lang: string, @Res() res) {
    this.translationService.export(lang).then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/search")
  async search(@Query("keyword") keyword: string, @Res() res) {
    this.translationService.search(keyword).then(ctx => res.json(ctx));
  }
}
