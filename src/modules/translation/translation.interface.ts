import { Document, Types, Schema } from 'mongoose';

export interface Translation extends Document {
  readonly featureId: Schema.Types.ObjectId;
  readonly text: string;
  readonly key: string;
  readonly en: string;
  readonly nl: string;
  draft: boolean;
}
