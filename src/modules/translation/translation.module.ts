import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { TranslationController } from "./translation.controller";
import { TranslationService } from "./translation.service";
import { TranslationSchema } from "./translation.schema";
import { FeatureSchema } from "../feature/feature.schema";
import { TranslationExport } from "./translation.export";
import { SocketService } from "../../socket.service";
import { TranslationSocket } from "./translation.socket";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Translation", schema: TranslationSchema },
      { name: "Feature", schema: FeatureSchema }
    ])
  ],
  controllers: [TranslationController],
  components: [
    TranslationService,
    TranslationExport,
    TranslationSocket,
    SocketService
  ]
})
export class TranslationModule {}
