import {
  IsString,
  IsInt,
  IsNumber,
  IsArray,
  IsDate,
  IsOptional,
  IsBoolean,
  IsMongoId
} from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class TranslationDto {
  @IsMongoId()
  @ApiModelProperty()
  readonly featureId: Types.ObjectId;

  @IsString()
  @ApiModelProperty()
  readonly text: string;

  @IsString()
  @ApiModelProperty()
  readonly key: string;

  @IsString()
  @ApiModelProperty()
  readonly en: string;

  @IsString()
  @ApiModelProperty()
  readonly nl: string;

  @IsOptional()
  @IsBoolean()
  @ApiModelProperty()
  readonly draft: boolean;
}
