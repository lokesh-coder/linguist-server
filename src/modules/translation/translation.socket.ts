import { Component, Inject } from "@nestjs/common";
import { SocketService } from "../../socket.service";

@Component()
export class TranslationSocket {
  constructor(private socket: SocketService) {}
  added(data) {
    this.socket.emit({
      key: "TRANSLATION_FEEDS",
      data: { type: "add", value: data }
    });
    return data;
  }
  deleted(data) {
    this.socket.emit({
      key: "TRANSLATION_FEEDS",
      data: { type: "delete", value: data }
    });
    return data;
  }
  updated(data) {
    this.socket.emit({
      key: "TRANSLATION_FEEDS",
      data: { type: "update", value: data }
    });
    return data;
  }
}
