import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res,
  Body,
  NotFoundException,
  HttpStatus,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Query
} from "@nestjs/common";
import { FeatureService } from "./feature.service";
import { FeatureDto } from "./feature.dto";
import {
  ApiUseTags,
  ApiOperation,
  ApiProduces,
  ApiImplicitParam,
  ApiImplicitHeader
} from "@nestjs/swagger";
import { Types, Schema } from "mongoose";
import * as parsePath from "parse-filepath";
import * as multer from "multer";
import * as fingerslug from "fingerslug";
import * as rootPath from "app-root-path";
import { normalize } from "path";
import { FeatureSocket } from "./feature.socket";

@ApiUseTags("feature")
@Controller("feature")
export class FeatureController {
  constructor(
    private readonly featureService: FeatureService,
    private socket: FeatureSocket
  ) {}
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/one/:id")
  async getOne(@Param("id") id: string, @Res() res) {
    this.featureService
      .getOne(id)
      .then(ctx => res.json(ctx))
      .catch(err => {
        res.status(HttpStatus.NOT_FOUND).send();
      });
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Get("/all")
  async getAll(@Res() res) {
    this.featureService.getAll().then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Post("/save-image")
  @UseInterceptors(
    FileInterceptor("file", { storage: FeatureController.fileStorage() })
  )
  async saveImage(@UploadedFile() file, @Query("id") id: string, @Res() res) {
    this.featureService
      .updateImage(id, file.filename)
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Post("/save")
  async save(@Body() body: FeatureDto, @Res() res) {
    this.featureService
      .save(body)
      .then(x => this.socket.added(x))
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Put("/one/:id")
  async update(@Param("id") id: string, @Body() body: FeatureDto, @Res() res) {
    this.featureService
      .update(id, body)
      .then(x => this.socket.updated(x))
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Delete("/one/:id")
  async delete(@Param("id") id: number, @Res() res) {
    this.featureService
      .deleteOne(id)
      .then(x => this.socket.deleted(x))
      .then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Delete("/all")
  async deleteAll(@Res() res) {
    this.featureService.deleteAll().then(ctx => res.json(ctx));
  }

  @ApiImplicitHeader({ name: "Authorization" })
  @Put("/remove-image")
  async deleteImage(@Param("id") id: number, @Res() res) {
    this.featureService.updateImage(id, null).then(ctx => res.json(ctx));
  }

  static fileStorage() {
    return multer.diskStorage({
      destination: function(req, file, cb) {
        cb(null, `${rootPath}/public/uploads`);
      },
      filename: function(req, file, cb) {
        let p = parsePath(file.originalname).extname;
        cb(null, fingerslug(file.originalname + p));
      }
    });
  }
}
