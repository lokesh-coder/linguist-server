import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Feature } from "./feature.interface";
import { FeatureDto } from "./feature.dto";
import { FeatureSchema } from "./feature.schema";
import { InjectModel } from "@nestjs/mongoose";

@Component()
export class FeatureService {
  constructor(
    @InjectModel(FeatureSchema) private readonly featureModel: Model<Feature>
  ) {}
  async getOne(id) {
    return await this.featureModel.findById(id).exec();
  }
  async getAll() {
    return await this.featureModel.find().exec();
  }
  async getAllWithTranslations() {
    return await this.featureModel
      .find()
      .populate("translations")
      .exec();
  }
  async update(id, data) {
    return await this.featureModel
      .findByIdAndUpdate(id, data, { new: true })
      .exec();
  }
  async deleteOne(id) {
    return await this.featureModel.findByIdAndRemove(id).exec();
  }
  async deleteAll() {
    return await this.featureModel
      .find()
      .remove()
      .exec();
  }
  async save(data: FeatureDto): Promise<Feature> {
    let featureData = new this.featureModel(data);
    return await featureData.save();
  }

  async updateImage(id, imagePath) {
    return await this.featureModel
      .findByIdAndUpdate(id, { image: imagePath }, { new: true })
      .select("image")
      .exec();
  }
}
