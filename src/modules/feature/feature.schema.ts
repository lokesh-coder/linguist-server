import * as mongoose from "mongoose";

export const FeatureSchema = new mongoose.Schema(
  {
    name: String,
    desc: String,
    key: String,
    url: String,
    image: String,
    translations: [
      { type: mongoose.Schema.Types.ObjectId, ref: "Translation" }
    ],
    draftTranslations: [
      { type: mongoose.Schema.Types.ObjectId, ref: "Translation" }
    ]
  },
  { timestamps: true }
);
