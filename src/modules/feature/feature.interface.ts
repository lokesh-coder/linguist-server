import { Document } from "mongoose";

export interface Feature extends Document {
  readonly name: string;
  readonly desc: string;
  readonly url: string;
  readonly key: string;
  readonly image: string;
  translations: string[];
  draftTranslations: string[];
  readonly createdDate: number;
}
