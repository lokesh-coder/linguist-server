import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { FeatureController } from "./feature.controller";
import { FeatureService } from "./feature.service";
import { featureProviders } from "./feature.provider";
import { FeatureSchema } from "./feature.schema";
import { FeatureSocket } from "./feature.socket";
import { SocketService } from "../../socket.service";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Feature", schema: FeatureSchema }])
  ],
  controllers: [FeatureController],
  components: [FeatureService, FeatureSocket, SocketService],
  exports: [FeatureService, FeatureSocket]
})
export class FeatureModule {}
