import {
  IsString,
  IsInt,
  IsNumber,
  IsArray,
  IsDate,
  IsOptional
} from "class-validator";
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class FeatureDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsString()
  @ApiModelProperty()
  readonly desc: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty()
  readonly key: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty()
  readonly url: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty()
  readonly image: string;

  @IsOptional()
  @IsString({ each: true })
  @ApiModelPropertyOptional()
  readonly translations: string[];

  @IsOptional()
  @IsString({ each: true })
  @ApiModelPropertyOptional()
  readonly draftTranslations: string[];

  @IsOptional()
  @IsDate()
  @ApiModelPropertyOptional()
  readonly createdDate: Date;
}
