FROM node:8.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm config set unsafe-perm true
RUN npm i -g npm@5.7.1 && npm ci
COPY . .
EXPOSE 3000
ENV MONGODB_HOST="mongo"
CMD NODE_ENV='production' node index.js