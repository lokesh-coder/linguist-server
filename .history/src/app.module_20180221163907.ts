import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppController } from "./app.controller";

import { DatabaseModule, ArticleModule } from "./modules";

@Module({
  imports: [MongooseModule.forRoot("mongodb://localhost/Blog"), ArticleModule],
  controllers: [AppController],
  components: []
})
export class ApplicationModule {}
