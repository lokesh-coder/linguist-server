import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";

import { DatabaseModule } from "./modules";

@Module({
  imports: [ArticleModule, DatabaseModule],
  controllers: [AppController],
  components: []
})
export class ApplicationModule {}
