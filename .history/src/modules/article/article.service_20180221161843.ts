import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Article } from "./article.interface";

@Component()
export class ArticlesService {
  constructor(
    @Inject("ArticleModelToken") private readonly articleModel: Model<Article>
  ) {}
  getOne() {}
  getAll() {}
  update() {}
  deleteOne() {}
  deleteAll() {}
  save() {}
}
