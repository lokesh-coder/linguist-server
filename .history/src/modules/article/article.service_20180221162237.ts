import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Article } from "./article.interface";
import { ArticleDto } from "./article.dto";

@Component()
export class ArticlesService {
  constructor(
    @Inject("ArticleModelToken") private readonly articleModel: Model<Article>
  ) {}
  async getOne() {}
  getAll() {}
  update() {}
  deleteOne() {}
  deleteAll() {}
  async save(data: ArticleDto): Promise<Article> {
    let articleData = new this.articleModel(data);
    return await articleData.save();
  }
}
