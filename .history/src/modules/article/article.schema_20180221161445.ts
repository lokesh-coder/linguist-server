import * as mongoose from "mongoose";

export const ArticleSchema = new mongoose.Schema({
  name: String,
  age: Number,
  breed: String
});
