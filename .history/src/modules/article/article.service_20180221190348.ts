import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Article } from "./article.interface";
import { ArticleDto } from "./article.dto";
import { ArticleSchema } from "./article.schema";
import { InjectModel } from "@nestjs/mongoose";

@Component()
export class ArticlesService {
  constructor(
    @InjectModel(ArticleSchema) private readonly articleModel: Model<Article>
  ) {}
  async getOne(id) {
    return await this.articleModel.findById(id).exec();
  }
  async getAll() {
    return await this.articleModel.find().exec();
  }
  async update(id, data) {
    return await this.articleModel
      .findByIdAndUpdate(id, data, { new: true })
      .exec();
  }
  async deleteOne(id) {
    return await this.articleModel.findByIdAndRemove(id).exec();
  }
  async deleteAll() {
    return await this.articleModel
      .find()
      .remove()
      .exec();
  }
  async save(data: ArticleDto): Promise<Article> {
    let articleData = new this.articleModel(data);
    return await articleData.save();
  }
}
