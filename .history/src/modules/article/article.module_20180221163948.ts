import { Module } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleController } from "./article.controller";
import { ArticlesService } from "./article.service";
import { articlesProviders } from "./article.provider";

@Module({
  imports:[MongooseModule.forFeature([{ name: 'Cat', schema: CatSchema }])]
  controllers: [ArticleController],
  components: [ArticlesService, ...articlesProviders],
  exports: [ArticlesService, ...articlesProviders]
})
export class ArticleModule {}
