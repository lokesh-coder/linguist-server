import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { Article } from "./article.interface";
import { ArticleDto } from "./article.dto";
import { ArticleSchema } from "./article.schema";
import { InjectModel } from "@nestjs/mongoose";

@Component()
export class ArticlesService {
  constructor(
    @InjectModel(ArticleSchema) private readonly articleModel: Model<Article>
  ) {}
  async getOne() {}
  getAll() {}
  update() {}
  deleteOne() {}
  deleteAll() {}
  async save(data: ArticleDto): Promise<Article> {
    let articleData = new this.articleModel(data);
    return await articleData.save();
  }
}
