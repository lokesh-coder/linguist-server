import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";
import { ArticleDto } from "./article.dto";

@Component()
export class ArticlesService {
  constructor(
    @Inject("ArticleModelToken")
    private readonly articleModel: Model<ArticleDto>
  ) {}
  getOne() {}
  getAll() {}
  update() {}
  deleteOne() {}
  deleteAll() {}
  save() {}
}
