import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res,
  Body
} from "@nestjs/common";
import { ArticlesService } from "./article.service";
import { ArticleDto } from "./article.dto";

@Controller("article")
export class ArticleController {
  constructor(private readonly articlesService: ArticlesService) {}
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get("/one/:id")
  async getArticle(@Param() params, @Res() res) {
    res.send(`got this, ${params.id}`);
  }

  @Get("/all")
  async getAllArticles(@Res() res) {
    this.articlesService.getAll().then(ctx => {
      res.json(ctx);
    });
  }

  @Post("/save")
  async saveArticle(@Body() body: ArticleDto, @Res() res) {
    this.articlesService.save(body).then(ctx => {
      res.json(ctx);
    });
  }

  @Put("/one/:id")
  async updateArticle(@Param() params, @Body() body: ArticleDto, @Res() res) {}

  @Delete(":id")
  async deleteArticle(@Param() params, @Res() res) {}

  @Delete("/all")
  async deleteAllArticles() {}
}
