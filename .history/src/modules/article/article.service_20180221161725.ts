import { Component, Inject } from "@nestjs/common";
import { Model } from "mongoose";

@Component()
export class ArticlesService {
  constructor(
    @Inject("ArticleModelToken") private readonly articleModel: Model<Article>
  );
  getOne() {}
  getAll() {}
  update() {}
  deleteOne() {}
  deleteAll() {}
  save() {}
}
