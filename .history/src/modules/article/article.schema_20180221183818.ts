import * as mongoose from "mongoose";

export const ArticleSchema = new mongoose.Schema({
  title: String,
  content: String,
  commentsCount: Number,
  draftComments: [Number],
  timestamp: { type: Date, default: Date.now }
});
