import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res,
  Body
} from "@nestjs/common";
import { ArticlesService } from "./article.service";
import { ArticleDto } from "./article.dto";

@Controller("article")
export class ArticleController {
  constructor(private readonly articlesService: ArticlesService) {}
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get("/one/:id")
  async getOne(@Param() params, @Res() res) {
    this.articlesService.getOne(params.id).then(ctx => res.json(ctx));
  }

  @Get("/all")
  async getAll(@Res() res) {
    this.articlesService.getAll().then(ctx => res.json(ctx));
  }

  @Post("/save")
  async save(@Body() body: ArticleDto, @Res() res) {
    this.articlesService.save(body).then(ctx => res.json(ctx));
  }

  @Put("/one/:id")
  async update(@Param() params, @Body() body: ArticleDto, @Res() res) {}

  @Delete("/one/:id")
  async delete(@Param() params, @Res() res) {}

  @Delete("/all")
  async deleteAll() {}
}
