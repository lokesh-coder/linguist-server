import { Module } from "@nestjs/common";
import { ArticleController } from "./article.controller";
import { ArticlesService } from "./article.service";
import { catsProviders } from "./article.provider";

@Module({
  controllers: [ArticleController],
  components: [ArticlesService, ...catsProviders],
  exports: [ArticlesService]
})
export class ArticleModule {}
