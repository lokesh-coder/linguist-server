import { Module } from "@nestjs/common";
import { ArticleController } from "./article.controller";
import { ArticlesService } from "./article.service";

@Module({
  controllers: [ArticleController],
  components: [ArticlesService],
  exports: [ArticlesService]
})
export class ArticleModule {}
