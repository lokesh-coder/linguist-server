import { Module } from "@nestjs/common";
import { ArticleController } from "./article.controller";
import { ArticlesService } from "./article.service";
import { articlesProviders } from "./article.provider";

@Module({
  controllers: [ArticleController],
  components: [ArticlesService, ...articlesProviders],
  exports: [ArticlesService]
})
export class ArticleModule {}
