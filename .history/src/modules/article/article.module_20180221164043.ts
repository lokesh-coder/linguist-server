import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ArticleController } from "./article.controller";
import { ArticlesService } from "./article.service";
import { articlesProviders } from "./article.provider";
import { ArticleSchema } from "./article.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Article", schema: ArticleSchema }])
  ],
  controllers: [ArticleController],
  components: [ArticlesService]
})
export class ArticleModule {}
