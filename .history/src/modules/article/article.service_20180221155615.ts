import { Component } from "@nestjs/common";

@Component()
export class ArticlesService {
  getOne() {}
  getAll() {}
  update() {}
  deleteOne() {}
  deleteAll() {}
  save() {}
}
