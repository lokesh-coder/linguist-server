import { Connection } from "mongoose";
import { ArticleSchema } from "./article.schema";

export const catsProviders = [
  {
    provide: "ArticleModelToken",
    useFactory: (connection: Connection) =>
      connection.model("Article", ArticleSchema),
    inject: ["DbConnectionToken"]
  }
];
