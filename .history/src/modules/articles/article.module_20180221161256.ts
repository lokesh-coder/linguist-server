import { Module } from "@nestjs/common";
import { ArticleController } from "./article.controller";

@Module({
  controllers: [ArticleController],
  components: [...databaseProviders],
  exports: [...databaseProviders]
})
export class ArticleModule {}
