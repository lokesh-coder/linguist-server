export class ArticleDto {
  readonly title: string;
  readonly content: string;
  readonly commentsCount: number;
  readonly draftComments: number[];
}
