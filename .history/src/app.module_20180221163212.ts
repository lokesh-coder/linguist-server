import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";

import { DatabaseModule, ArticleModule } from "./modules";

@Module({
  imports: [DatabaseModule, ArticleModule],
  controllers: [AppController],
  components: []
})
export class ApplicationModule {}
