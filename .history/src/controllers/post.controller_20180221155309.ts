import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res
} from "@nestjs/common";

@Controller("article")
export class PostController {
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  getArticle(@Param() Params, @Res() res) {}

  @Get()
  getAllArticles(@Res() res) {}

  @Post()
  saveArticle(@Req() Request, @Res() res) {}

  @Put(":id")
  updateArticle(@Param() Params, @Req() Request, @Res() res) {}

  @Delete(":id")
  deleteArticle(@Param() Params, @Req() Request) {}

  @Delete()
  deleteAllArticles() {}
}
