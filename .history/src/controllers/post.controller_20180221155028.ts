import { Get, Controller, Put, Delete, Post, Req } from "@nestjs/common";

@Controller("article")
export class PostController {
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get()
  getAllArticles() {}

  @Get()
  getArticle(@Req() Request) {}

  @Post()
  saveArticle(@Req() Request) {}

  @Put()
  updateArticle(@Req() Request) {}

  @Delete()
  deleteArticle(@Req() Request) {}

  @Delete()
  deleteAllArticles() {}
}
