import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res
} from "@nestjs/common";

@Controller("article")
export class PostController {
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  getArticle(@Param() params, @Res() res) {}

  @Get()
  getAllArticles(@Res() res) {}

  @Post()
  saveArticle(@Req() req, @Res() res) {}

  @Put(":id")
  updateArticle(@Param() params, @Req() req, @Res() res) {}

  @Delete(":id")
  deleteArticle(@Param() params, @Req() req, @Res() res) {}

  @Delete()
  deleteAllArticles() {}
}
