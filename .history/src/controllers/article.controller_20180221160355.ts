import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res,
  Body
} from "@nestjs/common";
import { ArticlesService } from "../services";
import { ArticleDto } from "../dto/article.dto";

@Controller("article")
export class ArticleController {
  constructor(private readonly articlesService: ArticlesService) {}
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  getArticle(@Param() params: number, @Res() res) {}

  @Get()
  getAllArticles(@Res() res) {}

  @Post()
  saveArticle(@Body() body: ArticleDto, @Res() res) {}

  @Put(":id")
  updateArticle(@Param() params, @Body() body: ArticleDto, @Res() res) {}

  @Delete(":id")
  deleteArticle(@Param() params, @Req() req, @Res() res) {}

  @Delete()
  deleteAllArticles() {}
}
