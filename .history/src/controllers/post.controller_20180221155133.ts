import { Get, Controller, Put, Delete, Post, Req, Param } from "@nestjs/common";

@Controller("article")
export class PostController {
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  getArticle(@Param() param) {}

  @Get()
  getAllArticles() {}

  @Post()
  saveArticle(@Req() Request) {}

  @Put()
  updateArticle(@Req() Request) {}

  @Delete()
  deleteArticle(@Req() Request) {}

  @Delete()
  deleteAllArticles() {}
}
