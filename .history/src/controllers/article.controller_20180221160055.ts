import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res,
  Body
} from "@nestjs/common";
import { ArticlesService } from "../services";

@Controller("article")
export class ArticleController {
  constructor(private readonly articlesService: ArticlesService) {}
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  getArticle(@Param() params, @Res() res) {}

  @Get()
  getAllArticles(@Res() res) {}

  @Post()
  saveArticle(@Body() body, @Res() res) {}

  @Put(":id")
  updateArticle(@Param() params, @Req() req, @Res() res) {}

  @Delete(":id")
  deleteArticle(@Param() params, @Req() req, @Res() res) {}

  @Delete()
  deleteAllArticles() {}
}
