import { Get, Controller, Put, Delete, Post, Req, Param } from "@nestjs/common";

@Controller("article")
export class PostController {
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  getArticle(@Param() Params, @Res() res) {}

  @Get()
  getAllArticles() {}

  @Post()
  saveArticle(@Req() Request) {}

  @Put(":id")
  updateArticle(@Param() Params, @Req() Request) {}

  @Delete(":id")
  deleteArticle(@Param() Params, @Req() Request) {}

  @Delete()
  deleteAllArticles() {}
}
