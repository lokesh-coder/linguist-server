import {
  Get,
  Controller,
  Put,
  Delete,
  Post,
  Req,
  Param,
  Res,
  Body
} from "@nestjs/common";
import { ArticlesService } from "../services";
import { ArticleDto } from "../dto/article.dto";

@Controller("article")
export class ArticleController {
  constructor(private readonly articlesService: ArticlesService) {}
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get(":id")
  async getArticle(@Param() params, @Res() res) {
    res.send(`got this, ${params.id}`);
  }

  @Get()
  async getAllArticles(@Res() res) {}

  @Post()
  async saveArticle(@Body() body: ArticleDto, @Res() res) {}

  @Put(":id")
  async updateArticle(@Param() params, @Body() body: ArticleDto, @Res() res) {}

  @Delete(":id")
  async deleteArticle(@Param() params, @Res() res) {}

  @Delete()
  async deleteAllArticles() {}
}
