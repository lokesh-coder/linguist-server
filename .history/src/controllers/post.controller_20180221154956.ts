import { Get, Controller, Put, Delete, Post, Req } from "@nestjs/common";

@Controller("article")
export class PostController {
  @Get()
  root(): string {
    return "Hello => Postcontroller!";
  }

  @Get()
  getAllArticles() {}

  @Get(@Req Request)
  getArticle() {}

  @Post()
  saveArticle() {}

  @Put()
  updateArticle() {}

  @Delete()
  deleteAllArticles() {}

  @Delete()
  deleteArticle() {}
}
