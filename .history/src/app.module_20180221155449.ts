import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";

import { ArticleController } from "./controllers";

@Module({
  imports: [],
  controllers: [AppController, ArticleController],
  components: []
})
export class ApplicationModule {}
