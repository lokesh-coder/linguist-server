import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";

import { ArticleController } from "./controllers";
import { ArticlesService } from "./services";

@Module({
  imports: [],
  controllers: [AppController, ArticleController],
  components: [ArticlesService]
})
export class ApplicationModule {}
