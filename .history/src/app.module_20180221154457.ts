import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";

import { PostController } from "./controllers";

@Module({
  imports: [],
  controllers: [AppController, PostController],
  components: []
})
export class ApplicationModule {}
